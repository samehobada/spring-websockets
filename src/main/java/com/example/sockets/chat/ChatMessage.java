package com.example.sockets.chat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @author Sergi Almar
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChatMessage {

	private String from;
	private String text;

	@Override
	public String toString() {
		return "ChatMessage [from=" + from + ", message=" + text + "]";
	}
}
