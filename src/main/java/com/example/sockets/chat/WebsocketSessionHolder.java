package com.example.sockets.chat;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketSession;

public class WebsocketSessionHolder {

	static {
		sessions = new HashMap<>();
	}

	// key - username, value - List of user's sessions
	private static Map<String, List<WebSocketSession>> sessions;

	public static Map<String, List<WebSocketSession>> getAllSessions() {
		return sessions;
	}

	public static void removeSession(String username, WebSocketSession session) {

		synchronized (sessions) {
			var userSessions = sessions.get(username);

			if (userSessions != null) {
				userSessions.remove(session);
			}
		}
	}

	public static void addSession(String username, WebSocketSession session) {
		synchronized (sessions) {
			var userSessions = sessions.get(username);
			if (userSessions == null)
				userSessions = new ArrayList<WebSocketSession>();

			userSessions.add(session);
			sessions.put(username, userSessions);
		}
	}

	public static void closeSessions(String username) throws IOException {
		synchronized (sessions) {
			var userSessions = sessions.get(username);
			if (userSessions != null) {
				for (var session : userSessions) {
					// I use POLICY_VIOLATION to indicate reason of disconnecting for a client
					session.close(CloseStatus.POLICY_VIOLATION);
				}
				sessions.remove(username);
			}
		}
	}

}