package com.example.sockets.chat;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketTransportRegistration;
import org.springframework.web.socket.handler.WebSocketHandlerDecorator;
import org.springframework.web.socket.handler.WebSocketHandlerDecoratorFactory;

import com.example.sockets.chat.specificuser.CustomHandshakeHandler;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {

	@Override
	public void configureMessageBroker(final MessageBrokerRegistry config) {
		config.enableSimpleBroker("/topic"); // create broker to allow send messages from server to client/s.
//		config.setUserDestinationPrefix("/user"); // default for user channels = /user/<topic path>
		config.setApplicationDestinationPrefixes("/app"); // the main end point for the socket system.
	}

	// .setAllowedOrigins("*")
	@Override
	public void registerStompEndpoints(final StompEndpointRegistry registry) {
		// add end point for user so user can send (like post request)
		registry.addEndpoint("/chat").setHandshakeHandler(new CustomHandshakeHandler()).withSockJS();// .setWebSocketEnabled(false);
	}
	
	@Override
	public void configureWebSocketTransport(WebSocketTransportRegistration registration) {
		registration.addDecoratorFactory(new WebSocketHandlerDecoratorFactory() {
			@Override
			public WebSocketHandler decorate(final WebSocketHandler handler) {
				return new WebSocketHandlerDecorator(handler) {
					@Override
					public void afterConnectionClosed(WebSocketSession session, CloseStatus closeStatus)
							throws Exception {
						String username = session.getPrincipal().getName();
						WebsocketSessionHolder.removeSession(username, session);
					}

					@Override
					public void afterConnectionEstablished(final WebSocketSession session) throws Exception {

						// We will store current user's session into WebsocketSessionHolder after
						// connection is established
						String username = session.getPrincipal().getName();
						WebsocketSessionHolder.addSession(username, session);

						super.afterConnectionEstablished(session);
					}
				};
			}
		});
	}

}
