package com.example.sockets.chat;

import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class MessageSenderService {

	@Autowired
	SimpMessagingTemplate simpMessagingTemplate;

	private static final String WS_MESSAGE_TRANSFER_DESTINATION = "/topic/message";
//	private List<String> userNames = new ArrayList<>();

	public void sendMessages() {

		List<String> userNames = new ArrayList<>(WebsocketSessionHolder.getAllSessions().keySet());

//		.values().stream()
//				.reduce(new ArrayList<WebSocketSession>(), (List<WebSocketSession> t, List<WebSocketSession> u) -> {
//					t.addAll(u);
//					return t;
//				}).forEach((item) -> {
//				});

		for (String userName : userNames) {
			onMessageReceivedFromAgent(userName);
		}
	}

	public void onMessageReceivedFromAgent(String userName) {
		String time = new SimpleDateFormat("HH:mm").format(new Date());
		simpMessagingTemplate.convertAndSendToUser(userName, WS_MESSAGE_TRANSFER_DESTINATION, new OutputMessage("Agent",
				"a message sent from agent with random string " + RandomStringUtils.randomAlphanumeric(10), time));
	}

//	public void addUserName(String username) {
//		userNames.add(username);
//	}

	public void onMessageReceivedFromClient(ChatMessage message, Principal principal) {
		log.info("message received from user: '" + message.getFrom() + "' with text: '" + message.getText() + "'");
	}

}
