package com.example.sockets.chat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Lazy(false)
@Component
public class Scheduler {
	@Autowired
	MessageSenderService messageSenderService;

	@Scheduled(fixedDelay = 3000, initialDelay = 0)
	public void schedulingTask() {
		log.info("Send messages with schedule");
		messageSenderService.sendMessages();
	}
}