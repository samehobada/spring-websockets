package com.example.sockets.chat;

import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class ChatController {

	@Autowired
	MessageSenderService messageSenderService;

	@MessageMapping("/chat")
	@SendToUser("/topic/message")
	public OutputMessage receive(ChatMessage message, Principal principal) throws Exception {
		log.trace("received message" + message.getText());
		String time = new SimpleDateFormat("HH:mm").format(new Date());
		return new OutputMessage(message.getFrom(), message.getText(), time);
	}
	

}
